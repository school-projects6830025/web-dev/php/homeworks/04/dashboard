<?php
require_once 'inc/db.php';
/** @var \PDO $db */

$errors = [];
$postText = '';
$postCategoryId = '';

if (!empty($_POST)) {
    if (!empty($_POST['category'])) {
        $postCategoryId = $_POST['category'];
        $categoryQuery = $db->prepare('SELECT * FROM categories WHERE category_id=:categoryId LIMIT 1;');
        $categoryQuery->execute([
            ':categoryId' => $postCategoryId
        ]);
        if ($categoryQuery->rowCount() == 0) {
            $errors['category'] = 'Zvolená kategorie neexistuje!';
            $_POST['category'] = '';
        }

    } else {
        $errors['category'] = 'Musíte vybrat kategorii.';
    }

    $postText = trim(@$_POST['text']);
    if (empty($postText)) {
        $errors['postText'] = 'Musíte zadat text příspěvku.';
    }

    if (empty($errors)) {
        if (isset($_GET['action'])) {
            if ($_GET['action'] == 'edit') {
                $editQuery = $db->prepare('UPDATE posts SET category_id=:categoryId, text=:text WHERE post_id=:postId');
                $editQuery->execute([
                    ':postId' => $_GET['postId'],
                    ':categoryId' => $postCategoryId,
                    ':text' => $postText
                ]);

            } else if ($_GET['action'] == 'add') {
                $saveQuery = $db->prepare('INSERT INTO posts (user_id, category_id, text) VALUES (:user, :category, :text);');
                $saveQuery->execute([
                    ':user' => 1,
                    ':category' => $_POST['category'],
                    ':text' => $postText
                ]);
            }
        }

        header('Location: start.php');
        exit();
    }
}

if ($_GET['action'] == 'add') {
    $pageTitle = 'Nový příspěvek';
} else {
    $pageTitle = 'Edit příspěvek';
}

include 'inc/header.php';
?>
    <form method="post">
        <div class="form-group">
            <label for="category">Kategorie:</label>
            <select name="category" id="category"
                    class="form-control <?php echo(!empty($errors['category']) ? 'is-invalid' : ''); ?>">
                <?php if ($_GET['action'] == 'edit' && empty($postCategoryId) ) {
                    $categoryQuery = $db->prepare('SELECT * FROM categories ORDER BY name;');
                    $categoryQuery->execute();
                    $dbCategories = $categoryQuery->fetchAll(PDO::FETCH_ASSOC);

                    $categoryQuery = $db->prepare('SELECT category_id FROM posts WHERE post_id=:postId;');
                    $categoryQuery->execute([
                        ':postId' => $_GET['postId']
                    ]);
                    $dbCategoryId = $categoryQuery->fetch(PDO::FETCH_ASSOC);

                    echo '<option value="all">--vyberte--</option>';
                    if (!empty($dbCategories)) {
                        foreach ($dbCategories as $category) {
                            echo '<option value="' . $category['category_id'] . '" ' . ($category['category_id'] == $dbCategoryId['category_id'] ? 'selected' : '') . '>' . htmlspecialchars($category['name']) . '</option>';
                        }
                    } else {
                        echo "Code BUG: no dbCategories";
                    }

                } else {
                    $categoryQuery = $db->prepare('SELECT * FROM categories ORDER BY name;');
                    $categoryQuery->execute();
                    $categories = $categoryQuery->fetchAll(PDO::FETCH_ASSOC);

                    echo '<option value="all">--vyberte--</option>';
                    if (!empty($categories)) {
                        foreach ($categories as $category) {
                            echo '<option value="' . $category['category_id'] . '" ' . ($category['category_id'] == $postCategoryId ? 'selected="selected"' : '') . '>' . htmlspecialchars($category['name']) . '</option>';
                        }
                    }
                }
                ?>
            </select>
            <?php
            if (!empty($errors['category'])) {
                echo '<div class="invalid-feedback" style="display: flex">' . $errors['category'] . '</div>';
            }
            ?>
        </div>

        <div class="form-group">
            <label for="text">Text příspěvku:</label>
            <?php
            if ($_GET['action'] == 'edit' && empty($postText) && empty($errors['postText'])) {
                    $postsQuery = $db->prepare('SELECT text FROM posts WHERE post_id=:postId;');
                    $postsQuery->execute([
                        ':postId' => $_GET['postId']
                    ]);
                    $dbText = $postsQuery->fetch(PDO::FETCH_ASSOC);
                    if (!empty($dbText)) {
                        echo '<textarea name="text" id="text"  class="form-control" >' . htmlspecialchars($dbText['text']) . '</textarea>';
                    } else {
                        echo "Code BUG: no dbText";
                    }

            } else {
                echo '<textarea name="text" id="text" class="form-control ' . (!empty($errors['postText']) ? 'is-invalid' : '') . '" >' . htmlspecialchars($postText) . '</textarea>';

                if (!empty($errors['postText'])) {
                    echo '<div class="invalid-feedback" style="display: flex">' . $errors['postText'] . '</div>';
                }
            }
            ?>
        </div>

        <button type="submit" class="btn btn-primary">uložit...</button>
        <a href="index.php" class="btn btn-light">zrušit</a>
    </form>
<?php
include 'inc/footer.php';