<?php
require_once 'inc/db.php';
/** @var \PDO $db */

include 'inc/header.php';

$query = $db->prepare('SELECT * FROM categories ORDER BY name;');
$query->execute();

$categories = $query->fetchAll(PDO::FETCH_ASSOC);
echo '<form method="get">
        <label for="category">Kategorie</label><br/>
        <select id="category" name="category">';
        echo '<option value="all">Všechny</option>';
foreach ($categories as $category) {
    echo '<option value="' . $category['category_id'] . '" ' . ((isset($_GET['category']) && $_GET['category'] == $category['category_id']) ? 'selected' : '') . '>' . $category['name'] . '</option>';
}
echo '</select>
        <button type="submit">Zobrazit kategorii</button>
    </form>';


if (!empty($_GET['category'])) {
    if ($_GET['category'] == 'all') {
        $query = $db->prepare('SELECT
                           posts.*, users.name AS user_name, users.email, categories.name AS category_name, categories.category_id AS category_id
                           FROM posts JOIN users USING (user_id) JOIN categories USING (category_id) ORDER BY updated DESC;');
        $query->execute();
    } else {
        $query = $db->prepare('SELECT
                           posts.*, users.name AS user_name, users.email, categories.name AS category_name, categories.category_id AS category_id
                           FROM posts JOIN users USING (user_id) JOIN categories USING (category_id) WHERE category_id=:categoryId ORDER BY updated DESC;');
        $query->execute([
            ':categoryId' => $_GET['category']
        ]);
    }

} else {
    $query = $db->query('SELECT
                           posts.*, users.name AS user_name, users.email, categories.name AS category_name, categories.category_id AS category_id
                           FROM posts JOIN users USING (user_id) JOIN categories USING (category_id) ORDER BY updated DESC;');
}

$posts = $query->fetchAll(PDO::FETCH_ASSOC);
if (!empty($posts)) {
    echo '<div class="row">';
    foreach ($posts as $post) {
        echo '<article class="col-12 col-md-6 col-lg-4 col-xl-3 border border-dark mx-1 my-1 px-2 py-1">';
        echo '  <div><span class="badge badge-secondary">' . htmlspecialchars($post['category_name']) . '</span></div>';
        echo '  <div>' . nl2br(htmlspecialchars($post['text'])) . '</div>';
        echo '  <div class="small text-muted mt-1">';
        echo htmlspecialchars($post['user_name']);
        echo ' ';
        echo date('d.m.Y H:i:s', strtotime($post['updated']));
        echo '  </div>';
        echo '<div class="row my-3">
          <a href="edit.php?action=edit&postId=' . $post['post_id'] . '" class="btn btn-light" style="position:relative;left:4%">Upravit příspěvek</a>
        </div>';
        echo '</article>';
    }
    echo '</div>';

} else {
    echo '<div class="alert alert-info">Nebyly nalezeny žádné příspěvky.</div>';
}

echo '<div class="row my-3">
          <a href="edit.php?action=add" class="btn btn-primary">Přidat příspěvek</a>
        </div>';

include 'inc/footer.php';